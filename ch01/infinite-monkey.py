"""
You may have heard of the infinite monkey theorem? 
The theorem states that a monkey hitting keys at random 
on a typewriter keyboard for an infinite amount of time 
will almost surely type a given text, such as the complete works of William Shakespeare. 
Well, suppose we replace a monkey with a Python function. 
How long do you think it would take for a Python function 
to generate just one sentence of Shakespeare? 
The sentence we’ll shoot for is: “methinks it is like a weasel”
"""


import random


def generateOne(strlen):
    alphabet = "abcdefghijklmnopqrstuvwxyz "
    res = ""
    for i in range(strlen):
        res = res + alphabet[random.randrange(27)]
    return res


def score(goal, teststring):
    numSame = 0
    for i in range(len(goal)):
        if goal[i] == teststring[i]:
            numSame = numSame + 1
    return numSame / len(goal)


def main():
    goalString = 'methinks it is like a weasel'
    newString = generateOne(28)
    best = 0
    newScore = score(goalString, newString)
    cnt = 0
    while newScore < 1:
        if newScore > best:
            print(newScore, newString)
            best = newScore
        newString = generateOne(28)
        newScore = score(goalString, newString)
        cnt = cnt + 1
        if cnt % 10000000 == 0:
            print("\n Best string in 5 iteration is:" + newString)



main()
